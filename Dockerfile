FROM alpine:3.9

RUN apk --no-cache add ca-certificates=20190108-r0 libc6-compat=1.1.20-r5

COPY . .

EXPOSE 9091

ENTRYPOINT [ "/car-challenge" ]