module gitlab.com/nespral/test

require (
	github.com/gorilla/mux v1.7.3
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20190809123943-df4f5c81cb3b // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20180127040603-bd5ef7bd5415 // indirect
	github.com/xeipuuv/gojsonschema v1.1.0
)
