package api

import (
	"fmt"
	"github.com/xeipuuv/gojsonschema"
	"io/ioutil"
)

var loadCarsSchemaLoader gojsonschema.JSONLoader
var journeySchemaLoader gojsonschema.JSONLoader

func InitSchemas(basePath string) {
	b, err := ioutil.ReadFile("." + basePath + "/loadcar.json")
	if err != nil {
		fmt.Print("There was an error loading schemas")
	}

	loadCarsSchemaLoader = gojsonschema.NewStringLoader(string(b))
	b, err = ioutil.ReadFile("." + basePath + "/journey.json")

	if err != nil {
		fmt.Print("There was an error loading schemas")
	}

	journeySchemaLoader = gojsonschema.NewStringLoader(string(b))
}
