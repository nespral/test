package api

import (
	"gitlab.com/nespral/test/service"
	"encoding/json"
	"github.com/xeipuuv/gojsonschema"
	"io/ioutil"
	"net/http"
)

func journey(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	bodyLoader := gojsonschema.NewStringLoader(string(body))
	result, err := gojsonschema.Validate(journeySchemaLoader, bodyLoader)

	if err != nil || result.Valid() == false {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	var j *service.Group
	if err = json.Unmarshal(body, &j); err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	journeyOp := service.JourneyOp{
		Group: j,
	}

	//Check channel is initialized
	service.JourneyChannel <- journeyOp
	response(w, http.StatusAccepted, JSONContentType)
}
