package api

import (
	"gitlab.com/nespral/test/service"
	"net/http"
	"strconv"
)

const IDQueryParam = "ID"

func dropoff(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	g := r.FormValue(IDQueryParam)
	groupId, err := strconv.Atoi(g)

	if err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	if groupId <= 0 {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	dropoffOp := service.DropoffOp{
		GroupId:         groupId,
		ResponseChannel: make(chan int),
	}

	service.DropoffChannel <- dropoffOp
	j := <-dropoffOp.ResponseChannel

	if groupId != j {
		response(w, http.StatusNotFound, JSONContentType)
		return
	}

	response(w, http.StatusNoContent, JSONContentType)
}
