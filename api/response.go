package api

import (
	"encoding/json"
	"net/http"
)

type errorResponse struct {
	Date    string `json:"date"`
	Message string `json:"message"`
}

func response(w http.ResponseWriter, code int, contentType string, body ...interface{}) {
	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(code)

	if body != nil {
		b, _ := json.Marshal(body[0])
		_, _ = w.Write(b)
	}
}
