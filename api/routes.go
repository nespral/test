package api

import (
	"github.com/gorilla/mux"
	"net/http"
)

const (
	JSONContentType = "application/json"
	FormContentType = "application/x-www-form-urlencoded"
)

func Routing() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/journey", validateJSONContentType(journey)).Methods("POST")
	r.HandleFunc("/dropoff", validateFormContentType(dropoff)).Methods("POST")
	r.HandleFunc("/locate", validateFormContentType(locate)).Methods("POST")
	r.HandleFunc("/awaiters", awaitingJourneys).Methods("GET")
	r.HandleFunc("/cars", validateJSONContentType(loadCarInventory)).Methods("PUT")
	r.HandleFunc("/status", status).Methods("GET")

	return r
}

func validateJSONContentType(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Content-Type") != JSONContentType {
			response(w, http.StatusBadRequest, JSONContentType)
			return
		}

		next.ServeHTTP(w, r)
	}
}

func validateFormContentType(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Content-Type") != FormContentType {
			response(w, http.StatusBadRequest, JSONContentType)
			return
		}

		next.ServeHTTP(w, r)
	}
}

