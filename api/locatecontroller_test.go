package api

import (
	"gitlab.com/nespral/test/service"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func Test_Locate_HappyPath(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	g := service.Group{
		Id:     1,
		People: 2,
	}
	carInventory := generateCars(1, 2)
	loadCars(carInventory)
	reserve(&g)
	statusCode, body := locateJourney(g.Id)
	car := service.Car{}

	if statusCode != http.StatusOK {
		t.Error("There was an error getting the car info by journey id")
	}

	if err := json.Unmarshal(body, &car); err != nil {
		t.Error("Error unmarshaling service response")
	}

	if carInventory[0].Id != car.Id {
		t.Error("Journey is not associated with the proper car")
	}

	if carInventory[0].Seats != car.Seats {
		t.Error("Journey is not associated with the proper car")
	}
}

func Test_Locate_GroupIsAwaiting_ReturnsNoContent(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	firstGroup := service.Group{
		Id:     1,
		People: 2,
	}

	secondGroup := service.Group{
		Id:     2,
		People: 2,
	}

	carInventory := generateCars(1, 0)
	loadCars(carInventory)
	reserve(&firstGroup)
	reserve(&secondGroup)

	statusCode, _ := locateJourney(secondGroup.Id)

	if statusCode != http.StatusNoContent {
		t.Error("The journey should be awaiting for a car")
	}
}

func Test_Locate_GroupDoesNotExist_ReturnsNotFound(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	firstGroup := service.Group{
		Id:     1,
		People: 2,
	}

	carInventory := generateCars(1, 0)
	loadCars(carInventory)

	statusCode, _ := locateJourney(firstGroup.Id)

	if statusCode != http.StatusNotFound {
		t.Error("The journey does not exist")
	}
}

func Test_Locate_InvalidGroup_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	carInventory := generateCars(1, 0)
	loadCars(carInventory)

	statusCode, _ := locateJourney(0)

	if statusCode != http.StatusBadRequest {
		t.Error("Zero is not a valid value")
	}
}

func locateJourney(journeyId int) (int, []byte) {
	request, _ := http.NewRequest(http.MethodPost, "", toFormData(IDQueryParam, strconv.Itoa(journeyId)))
	request.Header.Add("Content-type", FormContentType)
	recorder := httptest.NewRecorder()

	locate(recorder, request)
	return recorder.Code, recorder.Body.Bytes()
}
