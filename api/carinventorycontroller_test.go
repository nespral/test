package api

import (
	"bytes"
	"gitlab.com/nespral/test/service"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_LoadCars_HappyPath(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	statusCode := loadCars(cars)

	if statusCode != http.StatusOK {
		t.Error("There was an error loading the cars")
	}
}

func Test_LoadCars_EmptyArray_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(0, 0)
	statusCode := loadCars(cars)

	if statusCode != http.StatusBadRequest {
		t.Error("An empty array of cars must not be valid")
	}
}

func Test_LoadCars_ThereAreCarsWithMoreThan6Seats_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	cars[0].Seats = 20
	statusCode := loadCars(cars)

	if statusCode != http.StatusBadRequest {
		t.Error("Cars with more than 6 seats are not allowed")
	}
}

func Test_LoadCars_ThereAreCarsWithZeroOrLowerSeats_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	cars[0].Seats = 0
	statusCode := loadCars(cars)

	if statusCode != http.StatusBadRequest {
		t.Error("Cars with zero or less seats are not allowed")
	}
}

func Test_LoadCars_ThereAreCarsWithZeroOrLowerId_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	cars[0].Id = 0
	statusCode := loadCars(cars)

	if statusCode != http.StatusBadRequest {
		t.Error("Cars with zero or less ID are not allowed")
	}
}

func loadCars(cars []service.Car) int {
	request, _ := http.NewRequest(http.MethodPut, "", toByteArray(cars))
	request.Header.Add("Content-type", JSONContentType)
	recorder := httptest.NewRecorder()

	loadCarInventory(recorder, request)

	return recorder.Code
}

func toByteArray(data interface{}) *bytes.Buffer {
	b, _ := json.Marshal(data)
	return bytes.NewBuffer(b)
}

func generateCars(car2seats int, car4seats int) []service.Car {
	cars := []service.Car{}

	for i := 1; i <= car2seats; i++ {
		car := service.Car{
			Id:    i * 2,
			Seats: 2,
		}

		cars = append(cars, car)
	}

	for i := 1; i <= car4seats; i++ {
		car := service.Car{
			Id:    i * 16,
			Seats: 4,
		}

		cars = append(cars, car)
	}

	return cars
}

func setupTest(t *testing.T) func(t *testing.T) {
	initChannels()
	InitSchemas("/schemas")
	initManager()
	return func(t *testing.T) {
		service.QuitChannel <- true
	}
}

func initChannels() {
	service.LoadCarsChannel = make(chan service.LoadCarsOp)
	service.LocateChannel = make(chan service.LocateOp)
	service.DropoffChannel = make(chan service.DropoffOp)
	service.JourneyChannel = make(chan service.JourneyOp)
	service.AwaitingJourneysChannel = make(chan service.AwaitingJourneysOp)
	service.QuitChannel = make(chan bool)
}

func initManager() {
	go service.Manage(service.JourneyChannel, service.LocateChannel, service.DropoffChannel, service.LoadCarsChannel, service.AwaitingJourneysChannel, service.QuitChannel)
}
