package api

import (
	"gitlab.com/nespral/test/service"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_Journey_HappyPath(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	loadCars(cars)
	g := service.Group{
		Id:     1,
		People: 2,
	}

	statusCode := reserve(&g)

	if statusCode != http.StatusAccepted {
		t.Error("Reservation must be accepted")
	}
}

func Test_Journey_EmptyGroup_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	loadCars(cars)
	g := service.Group{}

	statusCode := reserve(&g)

	if statusCode != http.StatusBadRequest {
		t.Error("Bad Request")
	}
}

func Test_Journey_GroupWithNoId_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	loadCars(cars)
	g := service.Group{
		People: 4,
	}

	statusCode := reserve(&g)

	if statusCode != http.StatusBadRequest {
		t.Error("Bad Request")
	}
}

func Test_Journey_GroupWithNoSeats_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	loadCars(cars)
	g := service.Group{
		Id: 4,
	}

	statusCode := reserve(&g)

	if statusCode != http.StatusBadRequest {
		t.Error("Bad Request")
	}
}

func Test_Journey_GroupWithMoreThan6People_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	loadCars(cars)
	g := service.Group{
		Id:     4,
		People: 20,
	}

	statusCode := reserve(&g)

	if statusCode != http.StatusBadRequest {
		t.Error("Bad Request")
	}
}

func Test_Journey_GroupWithZeroPeople_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	cars := generateCars(2, 2)
	loadCars(cars)
	g := service.Group{
		Id:     4,
		People: 0,
	}

	statusCode := reserve(&g)

	if statusCode != http.StatusBadRequest {
		t.Error("Bad Request")
	}
}

func reserve(j *service.Group) int {
	request, _ := http.NewRequest(http.MethodPost, "", toByteArray(j))
	request.Header.Add("Content-type", JSONContentType)
	recorder := httptest.NewRecorder()

	journey(recorder, request)

	return recorder.Code
}
