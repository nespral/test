package api

import (
	"gitlab.com/nespral/test/service"
	"net/http"
	"strconv"
)

func locate(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	g := r.FormValue(IDQueryParam)
	groupId, err := strconv.Atoi(g)

	if err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	if groupId <= 0 {
		response(w, http.StatusBadRequest, "Invalid request")
		return
	}

	locateOp := service.LocateOp{
		GroupId:         groupId,
		ResponseChannel: make(chan *service.GroupStatus),
	}

	service.LocateChannel <- locateOp
	p := <-locateOp.ResponseChannel
	close(locateOp.ResponseChannel)

	if p == nil {
		response(w, http.StatusNotFound, JSONContentType)
		return
	} else if p.Status == service.Failed {
		response(w, http.StatusNoContent, JSONContentType)
		return
	}

	c := service.Car{
		Id:    p.CarId,
		Seats: p.Seats,
	}

	response(w, http.StatusOK, JSONContentType, c)
}

func awaitingJourneys(w http.ResponseWriter, r *http.Request) {
	awaitingJourneysOp := service.AwaitingJourneysOp{
		ResponseChannel: make(chan []service.Group),
		Dummy:           true,
	}

	service.AwaitingJourneysChannel <- awaitingJourneysOp
	awaitJourneys := <-awaitingJourneysOp.ResponseChannel
	close(awaitingJourneysOp.ResponseChannel)

	response(w, http.StatusOK, JSONContentType, awaitJourneys)
}
