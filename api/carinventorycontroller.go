package api

import (
	"gitlab.com/nespral/test/service"
	"encoding/json"
	"github.com/xeipuuv/gojsonschema"
	"io/ioutil"
	"net/http"
)

func loadCarInventory(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	bodyLoader := gojsonschema.NewStringLoader(string(body))
	result, err := gojsonschema.Validate(loadCarsSchemaLoader, bodyLoader)

	if err != nil || result.Valid() == false {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	var cars []service.Car
	if err = json.Unmarshal(body, &cars); err != nil {
		response(w, http.StatusBadRequest, JSONContentType)
		return
	}

	loadCarOp := service.LoadCarsOp{
		ResponseChannel: make(chan bool),
		Cars:            cars,
	}

	service.LoadCarsChannel <- loadCarOp
	<-loadCarOp.ResponseChannel
	close(loadCarOp.ResponseChannel)

	response(w, http.StatusOK, JSONContentType)
}
