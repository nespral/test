package api

import (
	"gitlab.com/nespral/test/service"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
)

func Test_Dropoff_HappyPath(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	g := service.Group{
		Id:     1,
		People: 2,
	}

	loadCars(generateCars(1, 0))
	reserve(&g)
	statusCode := dropoffJourney(g.Id)

	if statusCode != http.StatusNoContent {
		t.Error("There was an error doing the dropoff")
	}
}

func Test_Dropoff_InvalidGroupId_ReturnsBadRequest(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	g := service.Group{
		Id:     0,
		People: 2,
	}

	statusCode := dropoffJourney(g.Id)

	if statusCode != http.StatusBadRequest {
		t.Error("Zero is not a valid ID")
	}
}

func Test_Dropoff_GroupDoesntExist_ReturnsNotFound(t *testing.T) {
	tearDown := setupTest(t)
	defer tearDown(t)

	g := service.Group{
		Id:     4,
		People: 2,
	}

	statusCode := dropoffJourney(g.Id)

	if statusCode != http.StatusNotFound {
		t.Error("Group does not exist")
	}
}

func dropoffJourney(journeyId int) int {
	request, _ := http.NewRequest(http.MethodPost, "", toFormData(IDQueryParam, strconv.Itoa(journeyId)))
	request.Header.Add("Content-type", FormContentType)
	recorder := httptest.NewRecorder()

	dropoff(recorder, request)

	return recorder.Code
}

func toFormData(k, v string) io.Reader {
	d := url.Values{}
	d.Set(k, v)

	return strings.NewReader(d.Encode())
}
