package service

import "testing"

func Test_Locate_HappyPath(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Reserved {
		t.Error("Group must be traveling")
	}
}

func Test_Locate_AwaitingForSlot(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	secondGroup := Group{
		Id:     2,
		People: 2,
	}

	reserve(&newGroup)
	reserve(&secondGroup)

	group := locate(secondGroup.Id)

	if group.Status != Failed {
		t.Error("Group must be awaiting")
	}
}

func Test_Locate_GroupDoesntExist(t *testing.T) {
	group := locate(99)

	if group != nil {
		t.Error("Group must not be found")
	}
}
