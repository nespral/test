package service

import (
	"testing"
)

func Test_Reserve_HappyPath(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Reserved {
		t.Error("Group must be traveling")
	}
}

func Test_Reserve_NoSlotAvailable_GroupWillWait(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 4,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Failed {
		t.Error("Group must be awaiting for a car")
	}
}

func Test_Reserve_MultipleCarsAvailableButAssignsOneOfGroupSize(t *testing.T) {
	cars := generateCars(3, 1)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Reserved {
		t.Failed()
	}

	carSeats := carInventory[group.CarId]

	if carSeats != 2 {
		t.Error("Group was not assigned to a car of the appropiate number of seats")
	}
}

func Test_Reserve_GroupIsAlreadyTraveling_SecondBookingIsIgnored(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Reserved {
		t.Failed()
	}

	reserve(&newGroup)
	groupInfoAfterSecondBooking := locate(newGroup.Id)

	if groupInfoAfterSecondBooking.CarId != group.CarId {
		t.Error("Group didn't change car")
	}

	if groupInfoAfterSecondBooking.Status != Reserved {
		t.Error("Group must be traveling")
	}

	if groupInfoAfterSecondBooking.Status != group.Status {
		t.Error("Group status must not change after the second reservation")
	}
}

func Test_Reserve_GroupIsAlreadyAwaiting_SecondBookingIsIgnored(t *testing.T) {
	cars := generateCars(0, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Reserved {
		t.Failed()
	}

	reserve(&newGroup)
	groupInfoAfterSecondBooking := locate(newGroup.Id)

	if groupInfoAfterSecondBooking.Status != Failed {
		t.Error("Group must be awaiting")
	}
	if groupInfoAfterSecondBooking.Status != group.Status {
		t.Error("Group status must not change after the second reservation")
	}
}

func Test_Reserve_SlotAvailableButTheresAnotherGroupAwaiting_LastGroupWillWait(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	groupTraveling := Group{
		Id:     1,
		People: 2,
	}

	groupAwaiting := Group{
		Id:     2,
		People: 2,
	}

	lastGroup := Group{
		Id:     3,
		People: 2,
	}

	reserve(&groupTraveling)
	reserve(&groupAwaiting)
	reserve(&lastGroup)

	groupTravelingInfo := locate(groupTraveling.Id)
	groupAwaitingInfo := locate(groupAwaiting.Id)
	newGroupInfo := locate(lastGroup.Id)

	if groupTravelingInfo.Status != Reserved {
		t.Error("First group must be traveling")
	}

	if groupAwaitingInfo.Status != Failed {
		t.Error("Second group must be awaiting")
	}

	if newGroupInfo.Status != Failed {
		t.Error("Third group must be awaiting")
	}
}

func generateCars(car2seats int, car4seats int) []Car {
	cars := []Car{}

	for i := 1; i <= car4seats; i++ {
		car := Car{
			Id:    i * 16,
			Seats: 4,
		}

		cars = append(cars, car)
	}

	for i := 0; i < car2seats; i++ {
		car := Car{
			Id:    i * 2,
			Seats: 2,
		}

		cars = append(cars, car)
	}

	return cars
}
