package service

type (
	Group struct {
		Id     int `json:"id"`
		People int `json:"people"`
	}

	GroupStatus struct {
		Id             int
		Status         int
		RequestedSeats int
		CarId          int
		Seats          int
	}

	Car struct {
		Id    int `json:"id"`
		Seats int `json:"seats"`
	}
)

type (
	JourneyOp struct {
		Group *Group
	}

	DropoffOp struct {
		GroupId         int
		ResponseChannel chan int
	}

	LocateOp struct {
		GroupId         int
		ResponseChannel chan *GroupStatus
	}

	LoadCarsOp struct {
		Cars            []Car
		ResponseChannel chan bool
	}

	AwaitingJourneysOp struct {
		Dummy           bool
		ResponseChannel chan []Group
	}
)
