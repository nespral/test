package service

import "testing"

func Test_Dropoff_HappyPath(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	reserve(&newGroup)
	group := locate(newGroup.Id)

	if group.Status != Reserved {
		t.Error("Group must be traveling")
	}

	dropoff(newGroup.Id)
	group = locate(newGroup.Id)

	if group != nil {
		t.Error("Group must not exist after dropoff")
	}
}

func Test_Dropoff_CancelReservation(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	secondGroup := Group{
		Id:     2,
		People: 2,
	}

	reserve(&newGroup)
	reserve(&secondGroup)

	group := locate(secondGroup.Id)

	if group.Status != Failed {
		t.Error("Group must be awaiting for a car")
	}

	awaitingGroups := getAwaitingGroups()

	if len(awaitingGroups) != 1 {
		t.Error("Second group must be in the awaiting list")
	}
	dropoff(secondGroup.Id)
	group = locate(secondGroup.Id)

	if group != nil {
		t.Error("Group must not exist after dropoff")
	}

	awaitingGroups = getAwaitingGroups()

	if len(awaitingGroups) != 0 {
		t.Error("No groups must be in the awaiting list")
	}
}

func Test_Dropoff_FirstGroupReservesCarOfTwoSlots_SecondAndThirdReceiveThatSlotAfterDropoff(t *testing.T) {
	cars := generateCars(1, 0)
	loadCars(cars)

	newGroup := Group{
		Id:     1,
		People: 2,
	}

	secondGroup := Group{
		Id:     2,
		People: 1,
	}

	thirdGroup := Group{
		Id:     3,
		People: 1,
	}

	reserve(&newGroup)
	reserve(&secondGroup)
	reserve(&thirdGroup)

	awaitingGroups := getAwaitingGroups()

	if len(awaitingGroups) != 2 {
		t.Error("Two groups of one guy must be awaiting")
	}

	dropoff(newGroup.Id)

	awaitingGroups = getAwaitingGroups()

	if len(awaitingGroups) != 0 {
		t.Error("No groups should be in the awaiting list")
	}

	group := locate(secondGroup.Id)

	if group.Status != Reserved {
		t.Error("Second group must be traveling")
	}

	group = locate(thirdGroup.Id)

	if group.Status != Reserved {
		t.Error("Third group must be traveling")
	}
}
