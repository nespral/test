package service

var (
	zeroVal string
	zeroSum string
)

type awaitingJourneysList struct {
	Head *Node
	Tail *Node

	reporter bool
	len      int32
}

func (l *awaitingJourneysList) prepend(val *Group) (n *Node) {
	n = newNode(nil, l.Head, val)

	if l.Head != nil {
		l.Head.prev = n
	}

	if l.Tail == nil {
		l.Tail = n
	}

	l.Head = n
	l.len++
	return
}

func (l *awaitingJourneysList) append(val *Group) (n *Node) {
	n = newNode(l.Tail, nil, val)

	if l.Tail != nil {
		l.Tail.next = n
	}

	if l.Head == nil {
		l.Head = n
	}

	l.Tail = n
	l.len++
	return
}

// I decided that a compatible group is a group with a requested number of seats lower or equal to the seats passed as argument.
// This means that if Group1 that requested 1 seat and Group2 that requested 2 seats are awaiting and a 2 seat slot is released then Group1 will receive that slot because he asked it first.
func (l *awaitingJourneysList) findNextCompatibleGroup(seats int) (node *Node, err error) {
	currentNode := l.Head
	if currentNode == nil {
		return nil, nil
	}
	for currentNode != nil {
		//Any node with valid amount of space is OK
		if currentNode.group.People <= seats {
			return currentNode, nil
		}
		currentNode = currentNode.next
	}

	return nil, nil
}

func (l *awaitingJourneysList) findByGroup(groupId int) (node *Node, err error) {
	currentNode := l.Head
	if currentNode == nil {
		return nil, nil
	}
	for currentNode != nil {
		if currentNode.group.Id == groupId {
			return currentNode, nil
		}
		currentNode = currentNode.next
	}

	return nil, nil
}

func (l *awaitingJourneysList) getAll() []Group {
	currentNode := l.Head
	var journeys []Group
	if currentNode == nil {
		return nil
	}
	for currentNode != nil {
		j := Group{
			Id:     currentNode.group.Id,
			People: currentNode.group.People,
		}
		journeys = append(journeys, j)
		currentNode = currentNode.next
	}

	return journeys
}

func (l *awaitingJourneysList) remove(n *Node) {
	if n.prev != nil {
		n.prev.next = n.next
	} else {
		if l.Head = n.next; l.Head != nil {
			// remove the previous value from our new Head
			l.Head.prev = nil
		}
	}

	if n.next != nil {
		n.next.prev = n.prev
	} else {
		if l.Tail = n.prev; l.Tail != nil {
			l.Tail.next = nil
		}
	}

	n.prev = nil
	n.next = nil
	n.group = nil
	l.len--
}

func (l *awaitingJourneysList) length() (n int32) {
	return l.len
}

func newNode(prev, next *Node, val *Group) *Node {
	return &Node{prev, next, val}
}

// Node is a value container
type Node struct {
	prev *Node
	next *Node

	group *Group
}
