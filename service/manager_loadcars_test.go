package service

import "testing"

func Test_LoadCars_HappyPath(t *testing.T) {
	cars := generateCars(3, 6)
	loadCars(cars)

	totalCars := len(carInventory)

	if totalCars != 9 {
		t.Error("Car inventory must have 9 cars")
	}
}
