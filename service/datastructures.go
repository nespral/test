package service

var awaitingGroups awaitingJourneysList
var groupStatus map[int]GroupStatus
var carAvailableSeats map[int]int
var carInventory map[int]int
var carsBySeats = map[int]map[int]bool{}

var NotFound int = -1
var Failed int = 999
var Reserved int = 1

var maxSeats = 6

var LoadCarsChannel chan LoadCarsOp
var LocateChannel chan LocateOp
var DropoffChannel chan DropoffOp
var JourneyChannel chan JourneyOp
var AwaitingJourneysChannel chan AwaitingJourneysOp
var QuitChannel chan bool

func initChannels() {
	LoadCarsChannel = make(chan LoadCarsOp)
	LocateChannel = make(chan LocateOp)
	DropoffChannel = make(chan DropoffOp)
	JourneyChannel = make(chan JourneyOp)
	AwaitingJourneysChannel = make(chan AwaitingJourneysOp)
	QuitChannel = make(chan bool)
}
