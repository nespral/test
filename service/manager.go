package service

func Manage(
	journeyChannel <-chan JourneyOp,
	locateChannel <-chan LocateOp,
	dropoffChannel <-chan DropoffOp,
	loadCarsChannel <-chan LoadCarsOp,
	awaitingJourneysChannel <-chan AwaitingJourneysOp,
	quitChannel <-chan bool) {
	initialize()

	for {
		select {
		case journeyOp := <-journeyChannel:
			reserve(journeyOp.Group)

		case locateOp := <-locateChannel:
			locateOp.ResponseChannel <- locate(locateOp.GroupId)

		case dropoffOp := <-dropoffChannel:
			dropoffOp.ResponseChannel <- dropoff(dropoffOp.GroupId)

		case awaitingJourneysOp := <-awaitingJourneysChannel:
			awaitingJourneysOp.ResponseChannel <- getAwaitingGroups()

		case loadCarOp := <-loadCarsChannel:
			loadCars(loadCarOp.Cars)
			loadCarOp.ResponseChannel <- true

		case <-quitChannel:
			return
		}
	}
}

func locate(groupId int) (status *GroupStatus) {
	if _, f := groupStatus[groupId]; !f {
		return nil
	}

	s := groupStatus[groupId]
	return &s
}

func reserve(journey *Group) {
	//Check if this group is already waiting for seat or traveling
	g := locate(journey.Id)

	if g != nil {
		return
	}

	//Try to assign a slot checking available seats
	for i := journey.People; i <= maxSeats; i++ {
		//Check if there's available space
		CarsWithSizeI := carsBySeats[i]
		if CarsWithSizeI != nil && len(CarsWithSizeI) > 0 {
			for carId, _ := range CarsWithSizeI {
				assignCar(carId, journey)
				return
			}
		}
	}
	reservationFailed(journey)
}

func dropoff(journeyId int) int {
	g := locate(journeyId)
	//Group not traveling or awaiting. Dropoff is not possible
	if g == nil {
		return 0
	}
	//If journey didn't start then cancel it. Groups with status Failed are awaiting
	if g.Status == Failed {
		cancelJourney(journeyId)
	} else {
		//Check if there are other groups waiting that can use the already released slot
		newSlotSeats := finishJourney(g)
		signalAwaiters(g.CarId, newSlotSeats)
	}

	return journeyId
}

func assignCar(carId int, journey *Group) {
	availableSeats := carAvailableSeats[carId]
	CarsWithSizeA := carsBySeats[availableSeats]

	if CarsWithSizeA != nil && len(CarsWithSizeA) > 0 {
		for carId, _ := range CarsWithSizeA {
			delete(CarsWithSizeA, carId)
			gs := GroupStatus{
				Id:             journey.Id,
				Status:         Reserved,
				RequestedSeats: journey.People,
				CarId:          carId,
				Seats:          carInventory[carId],
			}

			groupStatus[journey.Id] = gs
			availableSeats = availableSeats - journey.People
			carAvailableSeats[carId] = availableSeats
			carsBySeats[availableSeats][carId] = true
			return
		}
	}
}

func reservationFailed(journey *Group) {
	gs := GroupStatus{
		Id:             journey.Id,
		Status:         Failed,
		RequestedSeats: journey.People,
	}
	groupStatus[journey.Id] = gs
	awaitingGroups.append(journey)
}

//This function could be recursive
func signalAwaiters(carId int, newSlotSeats int) {
	if awaitingGroups.length() > 0 {
		for {
			compatibleJourney, _ := awaitingGroups.findNextCompatibleGroup(newSlotSeats)
			if compatibleJourney != nil {
				newSlotSeats = newSlotSeats - compatibleJourney.group.People
				assignCar(carId, compatibleJourney.group)
				awaitingGroups.remove(compatibleJourney)
				if newSlotSeats > 0 && awaitingGroups.length() > 0 {
					continue
				}
			}
			break
		}
	}
}

func finishJourney(journey *GroupStatus) int {
	c := carAvailableSeats[journey.CarId]
	delete(carsBySeats[c], journey.CarId)
	newSlot := c + journey.RequestedSeats
	carAvailableSeats[journey.CarId] = newSlot
	carsBySeats[newSlot][journey.CarId] = true
	delete(groupStatus, journey.Id)
	return newSlot
}

func cancelJourney(journeyId int) {
	n, _ := awaitingGroups.findByGroup(journeyId)
	awaitingGroups.remove(n)
	delete(groupStatus, journeyId)
}

func loadCars(cars []Car) {
	initialize()
	for i := 0; i < len(cars); i++ {
		//Assume that some cars in the array can be duplicated (id). In case of dup we will pay attention to the first entry for that id. We will ignore the rest.
		// If we dont want to assume this then Quicksort array +  compare each element with its right neighbor.
		if _, f := carAvailableSeats[cars[i].Id]; f {
			continue
		}
		carsBySeats[cars[i].Seats][cars[i].Id] = true
		carAvailableSeats[cars[i].Id] = cars[i].Seats
		carInventory[cars[i].Id] = cars[i].Seats
	}
}

func getAwaitingGroups() []Group {
	return awaitingGroups.getAll()
}

func initialize() {
	carsBySeats = make(map[int]map[int]bool)
	carsBySeats[0] = make(map[int]bool)
	carsBySeats[1] = make(map[int]bool)
	carsBySeats[2] = make(map[int]bool)
	carsBySeats[3] = make(map[int]bool)
	carsBySeats[4] = make(map[int]bool)
	carsBySeats[5] = make(map[int]bool)
	carsBySeats[6] = make(map[int]bool)

	carInventory = make(map[int]int)
	carAvailableSeats = make(map[int]int)
	groupStatus = make(map[int]GroupStatus)
	awaitingGroups = awaitingJourneysList{}
}
