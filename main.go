package main

import (
	"gitlab.com/nespral/test/api"
	"gitlab.com/nespral/test/service"
	"net/http"
)

func main() {
	initChannels()
	go service.Manage(service.JourneyChannel, service.LocateChannel, service.DropoffChannel, service.LoadCarsChannel, service.AwaitingJourneysChannel, service.QuitChannel)

	api.InitSchemas("/api/schemas")
	http.ListenAndServe(":9091", api.Routing())
}

func initChannels() {
	service.LoadCarsChannel = make(chan service.LoadCarsOp)
	service.LocateChannel = make(chan service.LocateOp)
	service.DropoffChannel = make(chan service.DropoffOp)
	service.JourneyChannel = make(chan service.JourneyOp)
	service.AwaitingJourneysChannel = make(chan service.AwaitingJourneysOp)
	service.QuitChannel = make(chan bool)
}
